from src.airplane import Airplane


def test_starting_parameters():
    home_One = Airplane("Boing717", 500)
    assert home_One.total_distance_flown == 0
    assert home_One.number_of_occupied_seats == 0


def test_fly_method_once():
    home_One = Airplane("Boing717", 500)
    home_One.fly(5000)
    assert home_One.total_distance_flown() == 5000


def test_fly_method_twice():
    home_One = Airplane("Boing717", 500)
    home_One.fly(3000)
    home_One.fly(5000)
    assert home_One.total_distance_flown() == 8000


def test_is_service_required_false():
    home_One = Airplane("Boing717", 500)
    home_One.fly(9999)
    assert home_One.is_service_required() == False


def test_is_service_required_true():
    home_One = Airplane("Boing717", 500)
    home_One.fly(1001)
    assert home_One.is_service_required() == True

    # Samolot o maksymalnej ilości siedzeń 200 po załadowaniu 180 pasażerów ma dostępne 20 miejsc
    # Samolot o maksymalnej ilości siedzeń 200 po załadowaniu 201 pasażerów ma dostępne 0 miejsc i zajętych 200 miejsc


def test_board_passengers():
    home_One = Airplane("Boing717", 200)
    home_One.board_passangers(180)
    assert home_One.number_of_occupied_seats == 180


def test_overboard_passengers():
    home_One = Airplane("Boing717", 200)
    home_One.board_passangers(205)
    assert home_One.number_of_occupied_seats == 200


def test_get_available_seats_20_free_seats():
    home_One = Airplane("Boing717", 200)
    home_One.board_passangers(180)
    assert home_One.get_available_seats() == 20


def test_get_available_seats_no_free_seats():
    home_One = Airplane("Boing717", 200)
    home_One.board_passangers(201)
    assert home_One.get_available_seats() == 0
