class Airplane:
    def __init__(self, name, number_of_seats, total_distance_flown=0, number_of_occupied_seats=0):
        self.name = name
        self.total_distance_flown = total_distance_flown
        self.number_of_seats = number_of_seats
        self.number_of_occupied_seats = number_of_occupied_seats

    def fly(self, distance):
        self.total_distance_flown = self.total_distance_flown + distance

    def is_service_required(self):
        if self.total_distance_flown > 10000:
            return True
        else:
            return False

    def board_passengers(self, number_of_passengers):
        if self.number_of_seats >= self.number_of_occupied_seats + number_of_passengers:
            self.number_of_occupied_seats = self.number_of_occupied_seats + number_of_passengers
            return self.number_of_occupied_seats
        else:
            self.number_of_occupied_seats = self.number_of_seats
            return self.number_of_occupied_seats

    def get_available_seats(self):
        return self.number_of_seats - self.number_of_occupied_seats


if __name__ == '__main__':
    plane = Airplane('Boeing 707', 500, 8000)
    # print(plane.name)
    # print(plane.number_of_seats)
    # plane.fly(2000)
    # print(plane.is_service_required())
    # plane.board_passengers(700)
    print(plane.get_available_seats())
